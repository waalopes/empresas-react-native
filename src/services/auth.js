import * as fetch from 'isomorphic-fetch';
import { API_URL } from '../utils/constants';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const auth = (email, password) => fetch(
  `${API_URL}users/auth/sign_in`,
  {
    headers,
    method: 'POST',
    body: JSON.stringify({
      email,
      password,
    }),
  },
);
