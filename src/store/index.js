import { createStore, combineReducers } from 'redux';
import { auth } from './auth/reducer';

const reducers = combineReducers({
  auth,
});

export const store = createStore(reducers);
