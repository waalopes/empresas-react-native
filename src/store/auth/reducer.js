import {
  AUTH_USER_SUCCESS,
  AUTH_USER_ERROR,
} from './actions';

export const auth = (state, action) => {
  switch (action.type) {
    case AUTH_USER_SUCCESS:
      return {
        ...state,
        ...action.user,
      };
    case AUTH_USER_ERROR:
      return {
        ...state,
        ...action.user,
      };
    default:
      return { ...state };
  }
};
