import { auth } from '../../services/auth';

export const AUTH_USER_SUCCESS = 'AUTH_USER_SUCCESS';
export const AUTH_USER_ERROR = 'AUTH_USER_ERROR';

export const authUserSuccess = (user) => ({
  type: AUTH_USER_SUCCESS,
  user,
});

export const authUserError = (error) => ({
  type: AUTH_USER_ERROR,
  error,
});

export const authUser = (email, password) => (dispatch) => auth(email, password).then((user) => {
  dispatch(authUserSuccess(user));
}).catch((error) => {
  dispatch(authUserError(error));
});
