import React, { useState } from 'react';
import {
  Text, TextInput, Keyboard, Alert,
} from 'react-native';
import { useDispatch } from 'react-redux';

import {
  LoginContainer, FieldContainer, LoginButton, LoginTitle,
} from './styles';

import { authUser } from '../../store/auth/actions';

export default function App() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = () => {
    if (!email || !password) {
      return;
    }

    Keyboard.dismiss();
    useDispatch(authUser(email, password)).then((response) => {
      Alert.alert('Success', response);
    }).catch((err) => {
      Alert.alert('Error', err);
    });
  };

  return (
    <LoginContainer>
      <LoginTitle>Ioasys Assessment</LoginTitle>
      <FieldContainer>
        <Text>E-mail</Text>
        <TextInput
          textContentType="emailAddress"
          value={email}
          onChange={setEmail}
        />
      </FieldContainer>
      <FieldContainer>
        <Text>Password</Text>
        <TextInput
          textContentType="password"
          value={password}
          onChange={setPassword}
        />
      </FieldContainer>
      <LoginButton title="Login" onPress={handleSubmit} />
    </LoginContainer>
  );
}
