import styled from 'styled-components';

export const LoginContainer = styled.View`
    display: flex;
    height: 100%;
    align-items: center;
    justify-content: center;
`;

export const FieldContainer = styled.View`
    display: flex;
    flex-direction: column;
    margin: 10px;
`;

export const LoginButton = styled.Button`
    padding: 15px 10px;
    border: 1px solid #000;
    border-radius: 6px;
`;

export const LoginTitle = styled.Text`
    font-size: 20px;
    font-weight: bold;
`;
